# Wykład 5 - 24.10.2019

## Maski sygnałów - sigset
Trzeba uważać, żeby jak ustawiamy maskę nie usuwać wszystkiego, co było w niej do tej pory - parametr funkcji

## Oczekiwanie na sygnał
Trzy funkcje zachowujące się nieco inaczej. 

Sygnały realizowane w jeden sposób, ale obsługa ich może być na kilka sposobów.

Funkcja obsługi sygnału działa poza kontekstem, więc lepiej nie robić za dużo w niej. Na przykład można ustawić flagę i na nią zareagować.

## Sygnały niezawodne
Wstrzymywanie sygnału w czasie obsługi.

Niektóre sygnały mogą być kolejkowane - zdroworozsądkowe podejście.

## Grupy procesów
Proces może być członkiem grupy procesów. PGRP == PID to proces jest liderem grupy procesów.

Można dostarczać sygnały do wszystkich procesów w grupie.

Kiedy robimy pipe w shellu i przerywamy Ctrl+C, wtedy SIGINT wysyłany jest do wszystkich procesów w ramach pipe.

Terminal sterujący jest skojarzony z grupą procesów.

## Scheduler
Decyduje, który proces gotowy do wykonania uruchomić.

Wchodzi pomiędzy wywłaszczeniem starego, a uruchomieniem nowego procesu.

Tylko w stanie SRUN znajdzie się w kolejce schedulera, a wybierany jest proces o największym priorytecie.

Priorytety mogą się zmieniać dynamicznie.

Szeregowanie "time-shared" - systemy użytkowe i serwerowe. W systemach czasu rzeczywistego są też inne sposoby szeregowania, np. real-time. Potrzebne, żeby proces panował nad swoim wykonaniem (trzeba dobrze zaprogramować, żeby nie wywłaszczył procesora na stałe).

### Szeregowanie time-shared
Priorytet jest zmniejszany, jeśli działa długo na procesorze (CPU-bound). Będzie otrzymywał długi kwant czasu, ale rzadziej.

Jest podwyższany, jeśli często się usypia - np. w oczekiwaniu na IO (IO-bound). Będzie otrzymywał krótki kwant, ale często.

Priorytet jest sumą wartości 'nice' (użytkownika) oraz dynamicznej wspomnianej wcześniej.

## System plików
System plików jest drzewem. Jest jeden korzeń. Current dir jest dla każdego procesu.

Każdy proces może mieć przesunięty korzeń systemu plików, ale działa to tylko w dół. Przydatne np. przy serwisach sieciowych.

Typy plików:
- zwykłe (-)
- katalogi (d)
- sterowniki znakowe (c)
- sterowniki blokowe (b)
- FIFO (p)
- sym-link (l)
- socket (s)
- inne

Nazwa (nie jest atrybutem pliku) - nie może wystąpić / czy \0. Niezalecane znaki wzorce shella czy spacja.

Atrybuty plików:
- właściciel, grupa
- typ / prawa dostępu: owner (user), group, other, setuid, setgid, sticky
- daty-czasy: dostępu, modyfikacji, modyfikacji metryczki

Struktura domyślna folderów różna dla różnych dystrybucji. Zwykle znajdziemy: 'tmp', 'etc'.

Polecenia, które wydajemy do shella są programami.

Atrybuty (przy `ls -l`):
- typ
- prawa dostępu (owner (user) / group / others)
- liczba sztywnych dowiązań do pliku (wskazań z katalogów)
- user
- group
- rozmiar w B
- data-czas (3 warianty), typowo ostatnia modyfikacja
- nazwa pliku

Prawa dla katalogów:
- r - dostęp do nazw plików
- w - tworzenie, usuwanie, zmiana nazwy pliku
- x - dostęp do danych i metadanych pliku (wtedy działa operacja `cd`)

Sensowne jest ustawianie `r` razem z `x`.

### Ustawianie grupy, żeby pliki w katalogu ją dziedziczyły
Katalog `test1`, a grupa `cvs`.
``` bash
chmod g+s test1
chgrp cvs test1
```
Przydatne przy np. repozytoriach kodu, gdzie pracują użytkownicy różnych grup.

### Funkcja open()
Argument oflag ustawia tryb, w jakim otwiera się plik.

Łączenie flag poprzez operatora OR, np. O_RDWR | O_APPEND.

Jest ich dość dużo, ale nie wszystkie mają duży sens dla każdego typu pliku.

Argument mode określa prawa dostępu.

