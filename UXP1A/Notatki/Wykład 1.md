# Wykład 1
*03.10.2019*
## Organizacyjne
- Kolokwium 1 - po omówieniu IPC (~07.11)
- Kolokwium 2 - po całości materiału

O kolokwiach mówione co najmniej na dwa tygodnie przed

Projekt zaliczenie do 28.01 - start 7.11

dr inż. Grzegorz Blinowski,
http://ii.pw.edu.pl/~gjb,
g.blinowski@ii.pw.edu.pl,
pok. 315,
wtorki 10-12

Jeden plik ze slajdami na stronie ii

9.01 - plan poniedziałkowy i brak wykładu
100p w całości:
- 50p kolosy 25+25
- 50p projekt

Zaliczenie >= 50p:
- projekt zaliczony: zapisać się na niego, program się kompiluje i działa >= 25p
- suma kolokwiów > 20p.

Nie ma egzaminu, tylko kolosy i projekt.

Projekt w zespołach 3-osobowych - tematy podane na wykładzie:
- sprawozdanie wstępne i końcowe
- punktacja itp.

Punkty raczej w studia.elka, a w USOSie dopiero na końcu.

## Książki
"Advanced Programming in the UNIX Environment" W. Richard Stevens

"Programming with POSIX Threads" David R. Butenhof

## Plan wykładów
- Historia, Standardy
- Procesy
- Sygnały
- Pliki - podstawy
- Komunikacja IPC (potoki, FIFO, SYSV, IPC)
- *Kolos 1*
- Pliki - cd, zajmowanie plików i rekordów
- VFS, systemy plików
- VM - pamięć wirtualna
- Wątki
- XTI
- boot / init
- *Kolos 2*

Przedmiot traktuje w ogólności o interfejsie systemowym - API kernela.