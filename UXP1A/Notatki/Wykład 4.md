# Wykład 4 - 17.10.2019
## execve()
Ładuje i wykonuje nowy program do istniejącego procesu.

Wraca tylko przy błędzie.

Deskryptor procesu po wywołaniu execve():
- nadpisuje wybrane pola: dane, stos, sterta, text, registry
- zachowuje inne dane: PID, PPID, PGRP, grupy tablica plików, aktualny katalog, terminal sterujący (nie zachowuje funkcji obsługi sygnałów)

### Rodzina funkcji exec()
Jak mają końcówkę ...l to jest lista argumentów po pierwszym argumencie zakończona null-em.

W niektórych argumenty w postaci struktury tablicowej, a w niektórych listy następujących po sobie argumentów. Powód czysto użytkowy, żeby nie musieć deklarować tablicy i wpisywać w nią rzeczy. Np. `execl("/bin/ls", "ls", "-lR",0)`

W zmiennej PATH ścieżki przeszukiwania są rozdzielone `:`

Interpreter można sprecyzować za pomocą `#!`, np. `#!/bin/bash`

Środowisko można sobie wyobrazić jako obszar pamięci z listą zmiennych - komenda `env`. Przenoszone jest z procesu macierzystego.

Różnica między zmiennymi środowiskowymi, a zmiennymi shella - środowiskowe będą widoczne w podprocesach, a shella nie.

## Użytkownicy i grupy
### /etc/passwd
nazwa_użytkownika:hasło(x):uid:gid:home_folder:shell
### /etc/shadow
nazwa_użytkownika:hasło_hash(lub * jeśli nie można zalogować):...

/etc/passwd ma swoje API do pobierania użytkowników, ale w strukturze wyjściowej hasło zaszyfrowane dostanie tylko superuser

/etc/passwd może być zastąpiony sieciową bazą użytkowników NIS lub NIS+ - analogicznie do Windowsowego AD.

### /etc/group
nazwa:hasło(zawsze *):gid

### Real oraz effective user id
- Real UID/GID - identyfikacja, rozliczenia (RUID, RGID)
- Effective UID/GID - uprawnienia (EUID, EGID)
- Potrzeba zmiany UID dla root (przy logowaniu użytkownika)
- Potrzeba zmiany praw na innego użytkownika i powrotu do oryginalnych praw - szczelny system uprawnień
- Jest też "Saved UID / GID" - poniżej

#### Zamiana real i effective id - saved id

Typ |Przypadek 1    |Przypadek 2    |Przypadek 3    |Przypadek 4
----|---------------|---------------|---------------|-----------
R   |100            |100            |100            |0 -> 100
E   |100            |0              |0 <-> 100      |0 -> 100

Chcemy pozwalać na zamianę między id użytkownika i roota, gdy jakiś fragment wymaga "trybu" roota.

Root w 4. przypadku wchodzi jako dowolny użytkownik - `su`.

## Sygnały
Informacje binarne o zajściu zdarzenia dostarczane przez jądro do procesu.

Określony przez jego numer - typ/kod sygnału SIG...

Sygnał to reakcja na zdarzenia. Nie jest przerwaniem, a może reakcją na zdarzenia

### Synchroniczne
Proces coś zrobił i to wywołało sygnał, można się tego spodziewać.

### Asynchroniczne
Większość asynchroniczna. Niezależne od akcji procesu, nieoczekiwane, np. coś z klawiatury, pisanie do socketa bez odczytu (SIGPIPE).

Przyczyny sygnałów:
- Sam proces
- Inny proces
- Urządzenie (terminal, psaudoterminal)
- Gniazdo
- Liczniki czasu - sygnał za 10 sekund

Konsekwencje sygnałów:
- Wywołanie procedury obsługi
- Przerwanie procesu (zakończenie)
- Zatrzymanie procesu
- Zrzucenie pliku "core"

### Funkcja kill()
W prezentacjach wyjaśnienie jakie wartości argumentu PID wykonują jakie czynności.

### Funkcja obsługi sygnału
... i sigaction()