# Wykład 7 - 2019-11-07
## Potoki
Potok jest strukturą danych w jądrze.

Ma ograniczoną pojemność. Powinniśmy czytać <= PIPE_BUF, żeby zapis był atomowy.

Pipe jest potokiem nienazwanym i można tylko używać do komunikacji procesów spokrewnionych.

### Kolejki FIFO
Semantyka R/W identyczna jak dla potoków tworzonych przez pipe()

Identyfikowane przez pliki specjalne "widoczne" w systemie plików. Ale jest to bufor kernela.

``` bash
mkfifo fifo1
proga < fifo1 & progb > fifo1
```

Komunikacja jest w jedną stronę.