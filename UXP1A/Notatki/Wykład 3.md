# Wykład 3
## Czas
- utime - czas w trybie user
- stime - czas w trybie jądra

Uptime - ile system jest uruchomiony
Starttime - moment uruchomienia procesu (w ticknięciach - trzeba podzielić przez częstotliwość zegara)

Dużo czasu w trybie kernela dla procesu może oznaczać wysokie użycie IO.

## Hierarchia procesów
- Każdy ma rodzica, który go wywołuje.
- PIDy do 2^15-1 i przydzielane od dołu pierwszy wolny.
- Jak rodzic (pid 300) ginie, a dziecko ma ppid=300 (parent pid), to system podmieni ppid=1 (pid procesu init).

PID do identyfikacji, ustalenia hierarchii, zapisania do pliku (np. serwer Postfix'owy)

## API systemowe
```C
#include <unistd.h>

getppid();
getpid();
```
## Mapa pamięci procesu
W pustej przestrzeni między stosem, a bss (dane niezainicjowane) jest dynamicznie przydzielana sterta. Sterta rośnie w górę, a stos w dół. Nie zetkną się, gdyż ochrona pamięci.

Zmienne static przeżywają kolejne wywołania funkcji, ale są niewidoczne poza nią.

Komenda `nm` pozwala debugować pamięć, ale trzeba kompilować program z odpowiednią flagą. Zobaczymy zmienne globalne i statyczne.

Zmienna zainicjowana na 0 trafia do bss, jako niezainicjowana - optymalizacja.

W adresowaniu 32-bitowym pierwsze 3 GB na proces w trybie usera, a pozostały jest dostępny dopiero w trybie kernela.

### Adresowanie 64-bitowe
- ILP64 sprawiał, że int miał 64 bity, co sprawiało problemy w aplikacjach operujących na bitach (kryptorafia).
- LP64 najlepszym kompromisem między kompatybilnością i dostępnością 64 bitów. Int 32-bitowy, long 64-bitowy.
- LLP64 - najbardziej zachowawczy - int ma 32, long ma 32, a long long dopiero 64.

## Tworzenie procesów
### fork()
zwraca 0 dla potomka, > 0 czyli PID potomka dla procesu rodzica, < 0 dla błędu (za dużo procesów)
```C
if( (childpid = fork() ) == 0) {
    /* potomny */
} else {
    /* macierzysty */
}
```

Przydatne w architekturze watchdog-daemon. Watchdog pilnuje, żeby daemon zawsze działał, a daemon przetwarza.

### wait()
Proces rodzica oczekuje na proces potomny, zawsze pierwszy, który się skończy, a nie konkretny. Nie ma znaczenia czy najpierw wywołamy wait(), a potem się potomek skończy czy odwrotnie (deskryptor zombie potomka zostanie skonsumowany).

Ustawienie `signal( SIGCLD, SIG_IGN )` w rodzicu spowoduje, że zombie potomek nie wystąpi, bo informujemy, że rodzic nie jest zainteresowny oczekiwaniem na wait-cie.

`wait3()` ma 3 argumenty, w tym paramenr WNOHANG, powodujący, że się funkcja nie zawiesi.

`waitpid()` czeka na konkreny proces potomny

16-bitowy status zwracany przez wait(). 
|starsze 8-bitów|młodsze 8 bitów|scenariusz          |
|---------------|---------------|--------------------|
|arg dla exit() |0x00           |wyjście przez exit()|