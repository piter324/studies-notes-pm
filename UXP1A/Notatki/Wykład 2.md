# Wykład 2
*03.10.2019*
## Historia
Podział systemów UNIXowych po odejściu od numerowania arabskiego.

Pojawiło się wiele innowacji

IPC - Inter-Process Communication (pamięć dzielona, semafory, kolejki komunikatów)

Bałagan wynikający z podziału.

Lata 80-te wiele wersji na licencji AT&T

## Procesy
Jest kernel, który posiada moduły do wykonywania różnych czynności, np. moduł pamięci wirtualnej albo systemu plików.

Dwa poziomy:
1. poz. kernela - można wykonywać uprzywilejowane operacje
2. poz. użytkownika - nie może wykonywać pewnych operacji, bo nie jest uprzywilejowany

W mikrojądrze są moduły kernela, które nie muszą być w pełni uprzywilejowane.

### Podsystem procesów w jądrze
Proces - program w trakcie działania
Wejście do kernela przez:
- syscall
- przerwanie
- wyjątek

Szeregowanie - wybór procesu do wykonania. Odpowiada za to scheduler - podsystem systemu procesów

Przełączanie / wywłaszczanie - żeby proces nie mógł działać w nieskończoność na CPU

Pamięć, System plików, profilowanie, wyjątki

[Wykres trybu działania procesu w czasie]

#### Wywołanie systemowe (syscall)
Programista wywołuje funkcję systemową, np. open(), write()

Nie woła jej bezpośrednio, tylko przez funkcję biblioteczną, która przygotowuje stos i argumenty. Nie ma sensu bezpośrednio wołać.

#### Przerwanie
Tutaj program się nie wykonuje, a w pozostałych przypadkach wykonuje w trybie kernela. Przerwanie nie ma związku z procesem, a kernel działa sam dla siebie.

#### Wyjątek
Ogólnie to błąd - np. odwołanie do niedozwolonego adresu pamięci.

### Stan procesu
- SONPROC - proces się wykonuje
- SONPROC kernel - proces jest w trybie kernela
- SSLEEP - czeka na coś, np. zbuforowane dane z dysku, nie możemy go wykonywać
- SRUN - gotowy do wykonania
- SIDL (idle)
- SZOMB - większość zasobów zwolniona, ale deskryptor zostaje, aż ktoś go skonsumuje

### Stan procesu dla Linuxa
- TASK_RUNNING (running / ready but not running)
- TASK_INTERRUPTIBLE / TASK_UNINTERRUPTIBLE (np. gdy czeka)
- TASK_ZOMBIE
- TASK_STOPPED