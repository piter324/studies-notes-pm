# Wykład 6 - 31.10.2019
Sticky bit - nawet +w dla katalogu nie pozwala usuwać z niego plików

`dup()` i `dup2()` do komunikacji międzyprocesowej

Zawsze używany jest najniższy deskryptor dostępny dla procesu.

Jeśli w pliku przeniesiemy się poza obszar zapisany i zapiszemy to będzie dziura, która może nie podnieść faktycznego rozmiaru pliku, ale podniesie nominalny i przy kopiowaniu może zostać zaalokowany nominalny - tym samym kopia zajmie więcej niż plik źródłowy (przykład: pliki `core`).

Struktura `stat` z funkcji `stat()` o pliku zawiera informacje o nim, m.in. `st_blksize`, czyli rozmiar jednostki alokacji na urządzeniu z tym plikiem.

Dowiązania sztywne - logiczny sposób organizacji plików w UNIXie.

Dowiązania miękkie - dodatkowy mechanizm w systemie m.in. dla ułatwień.

Katalog mapuje nazwy plików na numery metryczek (inode). Nie przechowuje innych informacji o plikach.

Piusty katalog będzie fizycznie zawierał struktury linkujące do niego samego i do rodzica (`.`,`..`).

Numery metryczek unikalne dla systemu plików. Hard-linki tylko w obrębie jednego fs-a, bo oparte na metryczkach.

Symlinki są plikiem specjalnego typu, który jest napisem. Kernel znajdując symlinka, restartuje przetwarzanie ścieżki i zaczyna od zawartości symlinku. Nie sprawdza się praw do symlinków. Mogą przekraczać granice systemu plików. Obsługa symlinków wolniejsza, niż hard-linków.

Umask jest odejmowana od praw dostępu do pliku

Listowanie po katalogu nie jest atomowe, zapis do pliku jest, ale jak kilka procesów pisze w to samo miejsce, to w jakiejś kolejności dane będą nadpisane.

Można otrzymywać SIGIO po operacjach wykonanych na plikach w katalogu lub na samym katalogu.

`ioctl()` dla programowania urządzeń IO