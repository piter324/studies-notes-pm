# Spotkanie 1 - 15.10.2019

## Zaliczenie
- Prezentacja na zajęciach (PowerPoint lub PDF) - stary komputer do wyświetlania
- Artykuł napisany w LATeXu (2 tygodnie po prezentacji) - 4-6 stron - zgodnie z szablonem (2 kolumny)
- Udział w zajęciach i wypełnienie ankiet dotyczących prezentacji innych uczestników

Za niewypełnioną ankietę traci się 0.1 oceny, a za niewypełnienie 3 obniżana jest o pół oceny.

Ankiety wypełnione w ciągu trzech dni po prezentacji.

Co najmniej na 8 spotkaniach z 9.

Prezek na maila na tydzień przed.

Artykuł 2 tygodnie po prezce.

Założyć trzeba sobie konto w systemie ankietyzacji.

## Temat prezentacji
Chodzi o umiejętność prezentowania się, a zawartość merytoryczna jest mniej istotna. Nie musi być treścią pracy dyplomowej. Warto żeby było, żeby było blisko tematyki.

Minimalnie 10 minut. Maksymalnie 20-30 minut.

Wystąpienie na egzaminie dyplomowym ograniczone do 10 minut.

Może:   *Mikro back-end aplikacji internetowej na przykładzie rozwiązań opartych o języki Python i JavaScript*

Lub:    *Porównanie template engines po stronie serwera oraz klienta*

## Spóźnienie
Po pierwszym spóźnieniu zamykane mogą być drzwi.