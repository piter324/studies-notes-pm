# Spotkanie 2 - 22.10.2019
## Plusy
- Wyjaśnione terminy używane w prezentacji
- Schematy przydatne przy wyjaśnianiu zagadnień
- Przystępnie podany przykład zastosowania technologii
- Dobrze wyjaśniony kod

## Minusy
- Bardzo generyczne czcionki (czepialstwo ;) )
- Nieco nerwowe ruchy rękami i objawy zdenerwowania (np. poprawianie rękawów)
- Niepotrzebne przyznanie literówki na początku pokazania slajdu