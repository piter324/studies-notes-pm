# Wykład 6 - 31.10.2019
## Perspektywy
Perspektywa udaje tabelę do odczytu.

Może też zabezpieczać dostęp do danych. Perspektywa widzi tabele źródłowe z uprawnieniami jej twórcy, a użytkownik perspektywy może mieć prawa ograniczone do tabel, a do perspektywy nie.

Ograniczenie praw do modyfikowania tabel pod perspektywą tylko do widzialnych w perspektywnie kolumn.

## Synonimy
Można opatrzeć rzeczy definiowalne w bazie danych. Mogą być dla jednego użytkownika lub dla wszystkich.

Cele:
- ukrycie nazwy obiektu
- uproszczenia w przypadku obiektów odległych (poza nazwą jest jeszcze połączenie do bazy, więc dłuższe)
- uelastycznienie - zmiana nazwy czy lokalizacji obiektu wymaga tylko przedefiniowania synonimów

## Słownik systemowy
Zawiera definicje wszystkich obiektów z bazy danych.

Dostępne dla użytkownika przez perspektywy:
- USER...
- ALL...
- DBA...

`ALL.VIEWS` zawiera wszystkie perspektywy - zapytania do tego mogą pomóc w razie potrzeby

---

## Język PL/SQL
W latach 90. pojawiła się potrzeba programowania proceduralnego w ramach bazy danych.

W podstawowej wersji interpretowany.

Złożone typy danych to tablice asocjacyjne klucz-wartość.

Inicjalizacja pakietu wywoływana, gdy pierwsze wywołanie procedury w ramach pakietu.

Funkcje typu "utility" raczej jako procedury, a nie do pakietu, żeby całego nie ładować do pamięci.

Można zakodować wersję źródłową. Tak są zakodowane procedury wbudowane, żeby nie było prying eyes.

Funkcje i procedury się odpalają z prawami twórcy, ale tymi, które dostał explicite na siebie.

Kompilacja jest zautomatyzowana.

Tworzenie `CREATE [OR REPLACE] PROCEDURE|FUNCTION nazwa IS|AS ...

Tworzenie `CREATE [OR REPLACE] PACKAGE [BODY] nazwa IS|AS ...

Usuwanie `DROP PROCEDURE|FUNCTION|PACKAGE [BODY] nazwa;

Każdy program kończy się `/` w pierwszej linii.