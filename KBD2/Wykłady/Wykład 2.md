# Wykład 2
## Cechy systemu Oracle
### Uniwersalność
- Oracle jest uniwersalny (może służyć do stworzenia wielu rozwiązań, można dołączać opcje, typy danych, moduły kodu).
- Jest zgodny z ISO SQL, czyli entry-level. Dostęp do danych przez konkretne interfejsy, dostep przez HTTP.

### Przenośność
- Dostępne wersje na kilkadziesiąt platform.
- Wersje funkcjonalnie zgodne.
- Serwery i klienty współpracują ze sobą bez dodatkowych zabiegów.

### Skalowalność
- Systemy wieloprocesorowe
- Jedna baza przez kilka serwerów
- Praca w kratach (grid) i chmurze (cloud)
- Rozpraszanie bazy
- Inteligentne partycjonowanie danych

### Duża wydajność
- Zaawansowany kosztowy optymalizator zapytań
- Dzielone zasoby dla powtarzanych zapytań

### Wysoka dostępność
- Praca bez przerw
- Backup on-line
- Modyfikacja struktur i konwersje danych "na żywo"

### Bezpieczeństwo
- Różne prawa dostępu
- Zaawansowane mechanizmy bezpieczeństwa sieciowego

### Niezawodność
- Automatyczne odtwarzanie po awariach
- Zaawansowane mechanizmy kopii rezerwowych
- Serwery rezerwowe (Data Guard)
- Praca w kracie (grid) z migracją usług

Wiele opcji dostępnych w edycji Enterprise. **Partycjonowanie** bardzo istotne.

## Podstawowe zastosowania
### Systemy transakcyjne OLTP
Systemy transakcyjnego przetwarzania danych. Współdziałanie z monitorami transakcyjnymi, które potrafią wycofywać transakcje w systemie plików.

### Składowanie dokumentów i content management
- XML DB - udaje bazę danych przechowującą dokumenty XMLowe.
- Wyszukiwanie pełnotekstowe

### Hurtownie danych
Mechanizmy podnoszące efektywność:
- Indeksy bitowe
- Partycjonowanie
- Optymalizacja zapytań gwiaździstych

Narzędzia do procesów ETL - pobieranie danych do hurtowni

## Architetury systemów z DBMS Oracle
- Jednozadaniowa - program łączy się z DBMS wprost przez mechanizmy OSa
- Dwuzadaniowa - łączymy się z pośrednikiem, a ten z DBMS
- Klient-serwer - klient przez sieć -> proces nasłuchu DBMS
- Trójwarstwowa - mechanizm WWW -> serwer aplikacji -> proces nasłuchu DBMS
- Obiektowa rozproszona
- Kratowa - baza i serwery aplikacyjne rozproszone w kracie

## Produkty związane z DBMS
- Konsole SQL: SQL*Plus, iSQL*Plus
- Narządzie dla projektanta SQL i PL/SQL: SQL Developer
- Narzędzie dla projektanta baz danych: SQL Developer Data Modeler
- Narządzia administracyjne: Oracle Enterprise Manager
- Narzędzia pomocnicze:
    - SQL*Loader - ładowanie danych z dowolnego formatu
    - Import, Export - wypisują do plików w formacie ich wewnętrznym
    - Oracle Data Pump - zrzuca dane do plików na serwerze
- Interfejsy do języków programowania:
    - OCI (Oracle Call Interface) - API niskopoziomowe do bazy danych
    - ODBC
    - JDBC
    - Oracle Programmer - preprocesory, np. Pro*C - wprowadza się zapytania SQL w kod programu, który się kompiluje i pobiera dane