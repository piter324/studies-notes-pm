# Wykład 4 - 17.10.2019
## Programowanie proceduralne w PL/SQL
Elementy proceduralne: procedury, funkcje, pakiety, wyzwalacze
Zalety: przenośność, przetwarzanie na serwerze, stosunkowo dobra integracja z SQL

Można ewentualnie programować w Javie - poza darmową wersją. Rzadko używane.

## NLS - National Language Support
Przed stosowaniem UTF-8 były problemy z kompromisem pomiędzy wsparciem wielojęzykowym, a oszczędnością pamięci.

Zależy od niego strona kodowa, komunikaty, sortowanie, format liczb i dat

### Ustawienia NLS
Zmienne systemowe:
- NLS.LANG - język.terytorium.strona_kodowa
- zmienne pomocnicze, np. NLS.SORT, NLS.NUMERIC.CHARACTERS

Wyznaczają język, sortowanie, format domyślny liczb i dat

ALTER SESSION zmienia parametry NLS (oprócz strony kodowej) dla sesji

Zmienne te definiuje się w miejscu sensownym dla systemu operacyjnego, np. zmienne środowiskowe dla UNIXowych czy rejestr dla Windy

**----- KONIEC CZĘŚCI PIERWSZEJ - WSTĘPU -----**

## Język SQL
Rodzaje zdań:
- DQL - Data Query Language - zapytania SELECT, służące do pozyskania danych
- DML - Data Manipulation Language - INSERT, UPDATE, DELETE
- DDL - Data Definition Language - CREATE, ALTER, DROP - autocommity całych transakcji w momencie wywołania któregoś z DDL

## Środowiska wykonywania języka SQL
- Konwersacyjne:
    - konsole SQL
    - programy administracyjne (Enterprise Manager)
    - programy do graficznego tworzenia zapytań (np. Oracle Data Query)
- Języki programowania:
    - zanurzony SQL (embedded SQL) - w języku ogólnego przeznaczenia umieszczamy czyste zdania SQLowe. Przepuszczamy przez preprocesor i dopiero wtedy przez kompilator.
    - OCI (Oracle Call Interface)
    - ODBC
    - JDBC
- Narzędzia czwartej generacji (Oracle Developer, HTML DB - Application Express)
- Programy powszechnego użytku (przez ODBC), np. Microsoft Office z MS Query.

W aplikacjach biznesowych embedded SQL jest wygodniejszy i czytelniejszy od API (ODBC, JDBC, OCI).

## Obsługa błędów
- Sygnalizowane komunikatem do operatora jeśli nieobsłużone w programie, wyzwalaczu itp.
- Transakcja może być kontynuowana, a nie automatycznie wycofywana

## DQL - specyficzne cechy Oracle
- Słowo AS niedozwolone przed aliasami tabel
- AS nieobowiązkowe przed aliasami kolumn
- Aliasy kolumn niedozwolone (sic!) w klauzulach WHERE, GROUP BY, HAVING (aaaargh!)
- Dostępne operatory mnogościowe UNION, MINUS, INTERSECT
- Tabela - zaślepka DUAL, np. `SELECT SYSDATE FROM DUAL`;

## Złączenia wg SQL'99
- CROSS JOIN - produkt kartezjański
- LEFT|RIGHT|FULL OUTER JOIN - złączenia zewnętrzne
- JOIN ... ON - złączenie z jawnym podaniem warunku złączenia
- NATURAL JOIN - samodobór kolumn i dokonanie złączenia
- JOIN ... USING - równościowe ze wskazaniem kolumn
- brak KEY JOIN biorącego naturalnie klucze obce

## Podzapytania w klauzuli FROM
Są.

## Podzapytania w SELECT
Musi zwracać wartość skalarną, żeby udawać zapytanie.

## Wyrażenia w SQL
Może zawierać wywołania funkcji użytkownika - one nie mogą nic zmieniać, bo niezgodne ze standardem i się nie wykona.

Datę można zapisać znakowo, ale format może się nie zgadzać, więc warto zrobić jawną konwersję TO.DATE('1997-10-24','YYYY-MM-DD')

CURVAL podaje ostatnią wartośc dla sesji

NULLowe shenanigans na slajdzie.

Operatory też na slajdach.

Konwersje jawne (przez stosowne funkcje) mają możliwość sprecyzowania formatu.

Data i czas w NLS nie mają odmienionych miesięcy w polsku.

Kilka przydatnych funkcji wbudowanych.

DECODE i CASE - czytelniejszy

### Zapytanie hierarchiczne
Budowa drzewa hierarchicznego. Można sortować według równorzędnych gałęzi.