# Wykład 3
## Oracle Net
Komunikacja z bazą oraz między instancjami.

TNS alias - techniczne info o połączeniu z serwerem. Użycie jest dobre, bo pozwala bardziej flexible configuration, niż po prostu podawanie szczegółów serwera w aplikacji, np. przełączenie na serwer zapasowy, jeśli główny padnie.

## SQL*Plus
Żywa skamielina i niewygodny. Ale dobry do pojedynczych poleceń i skryptów.

## Organizacja logiczna bazy
Jeden program serwera na jedną bazę danych.
- Schemat - przestrzeń nazw, żeby tabele w jednej bazie o tej samej nazwie dla różnych klientów mogą się tak samo nazywać. Przy tworzeniu bazy są już co najmniej dwa schematy:
    - SYS - słownik systemowy, definiujący wszystko, co w bazie (właścicielem użytkownik SYS)
    - SYSTEM - domyślny administrator DBA (Database Administrator)
    - ...schematy użytkowników

Trzeba tworzyć użytkowe struktury danych w schematach użytkowych.

### Obiekty w bazie
1. Grono (cluster) - pozwala sterować fizyczną strukturą danych zależnie od ich treści - te same klucze obok siebie (silna i wyspecjalizowana optymalizacja)
1. Tabele
    1. Indeksy
    1. Wyzwalacze
    1. Więzy
1. Sekwencje - np. do generowanie liczb na klucze
1. Perspektywy i synonimy
1. Połączenia baz danych, migawki (perspektywy zmaterializowane)
1. Funkcje, procedury i pakiety
1. Typy, tabele, perspektywy obiektowe
1. Dokumenty w XML repository

### Nazwy
#### Zwykłe
- do 30 znaków - od litery, a dalej litery i cyfry
- bez znaków narodowych
- bez spacji
- wielkość liter nieistotna - w słownikach bazy i tak wielkimi literami

#### W cudzysłowie
- dowolne znaki, spacje i rozróżnianie liter
- nie stosować bez wyraźnego powodu
- aliasy nazw kolumn
- aliasy w zapytaniach SQLX tworzące XML
- zwracać uwagę w skryptach czy nie są niepotrzebnie zwrócone czy używane

### Typy danych
1. CHAR(n) - dobija automatycznie spacjami do n znaków
1. VARCHAR2(n) - zmiennej długości do n znaków (2 w nazwie typu, ze względu na inne rozwiązania od wersji 1 SQLowej - typ 2 niezgodny z VARCHAR zdefiniowanym w SQL, gdzie dana może być NULL, puste lub napis, w VARCHAR2 NULL i pusty są tożsame. System automatycznie tworzy VARCHAR2, nawet poproszony o VARCHAR)
1. NCHAR(n) i NVARCHAR2(n) - inną stronę kodową (lokalną) od strony CHAR i VARCHAR2
1. NUMBER(p,s) - parametry opcjonalne, p to precyzja (maksymalna liczba cyfr bez minusa i cyfr dziesiętnych), s to skala (liczba cyfr dziesiętnych), gdzie ujemna oznacza liczbę zer przed znakiem dziesiętnym. Stałoprzecinkowa liczba.
1. NUMBER(p) - liczba całkowita. Tego używamy do sztucznych kluczy, żeby nie były zmiennoprzecinkowe.
1. NUMBER() - liczba zmiennoprzecinkowa tak, jak zapisana. Wszystkie numbery do 38 cyfr.
1. FLOAT(p) - p to liczba bitów używanych w reprezentacji liczby zmiennoprzecinkowej.
1. BINARY FLOAT / BINARY DOUBLE - Numeryczne zmiennoprzecinkowe pojedynczej / podwójnej precyzji.
1. DATE - czas i data z dokładnością do sekund. Arytmetyka "na dniach" - np. 1 godzina to 1/24
1. TIMESTAMP - czas i data z dokładnością do części sekund
1. TIMESTAMP WITH TIMEZONE - nie może występować jako klucz główny
1. TIMESTAMP WITH LOCAL TIMEZONE - lokalna dla serwera, może być kluczem głównym
1. INTERVAL - odstęp między dwoma timestampami, zgodne z ISO.

#### Specjalne typy "relacyjne"
1. ROWID - typ przechowujący adres wiersza
1. UROWID - przechowuje drogę do wiersza, np. jeśli tabela jest index-organized to prowadzi po drzewie do tego wiersza
1. CLOB - bardzo długie teksty
1. RAW - dane binarne do 2000 bajtów
1. BLOB - wielkie dane binarne
1. BFILE - wielkie dane binarne w plikacj poza bazą

1. autoincrement - od wersji 12c klauzula `GENERATED ALWAYS AS IDENTITY`
1. brak BOOLEAN - wartości logicznej

#### Typy "LOB"
- W kolumnie jest *locator* i on jest zwracany w zapytaniach - optymalizacja względem inlinowego umiejscowienia

#### Warning: Typy ISO SQL
- Nie ma typów INTEGER i VARCHAR.
- Istnieją tylko dla źle rozumianej kompatybilności
- INTEGER zostanie zrzutowany na NUMBER(), czyli jest **ZMIENNOPRZECINKOWY**

### Więzy deklaratywne
#### Rodzaje
- NOT NULL
- klucze główne
- k. unikalne
- k. obce
- więzy kontrolne (check constraint)

#### Zalety
- niemożliwe do obejścia
- statyczne - dotyczą wszystkich wierszy
- super zoptymalizowane
- nie wymagają programowania

#### Wady
- ograniczona siła wyrazu - jest ich kilka tylko
- klucze obce nie mogą działać w rozproszonej bazie danych (wtedy proceduralnie)

**Uwaga!** Dla kluczy głównych i unikalnych automatycznie zostanie stworzony indeks.