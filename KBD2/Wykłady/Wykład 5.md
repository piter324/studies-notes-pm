# Wykład 5 - 24.10.2019
## PIVOT
Dla tabeli przestawnej. Jakiś wynik zależy od niezależnych czynników, np. godziny, które wykładowcy spędzili prowadząc wykłady dla danych przedmiotów.

## Zapytanie z klauzulą WITH
Tworzy tymczasową perspektywę tylko na czas zapytania, do której następnie pisanie trudnych zapytań jest dużo prostsze.

Na takiej perspektywie można wykonać DMLowe polecenia, ale musi to być perspektywa modyfikowalna. Np. jeśli jest zgrupowana, uśredniona itp. to nie ma to sensu i jest niemożliwe.

TRUNCATE TABLE nazwa [DROP STORAGE] - szybki delete, ale to jest tak naprawdę DDL (więc się autocommituje).

## Blokady
Są systemy, w które konflikty wpisane są w ich naturę. Na przykład rezerwacje.

Określamy rozsądnie nieduży obręb do blokowania - np. po wyborze pokładu i sekcji blokujemy kajuty w tej sekcji.

W Oracle blokujemy, stosując SELECT ... FOR UPDATE OF ... ale to jest kosztowne, więc trzeba rozsądnie zakładać.

Na zwolnienie blokady można oczekiwać z klauzulą WAIT, na dowolnym zdaniu, który zakłada blokadę.

## Klauzula DEFAULT
Wartość ustalana przy definicji tabeli i ona będzie użyta, jeśli w INSERT nie ma innej, sprecyzowanej. Można też podać słowo DEFAULT przy INSERT, żeby kwerenda była bardziej czytelna.

## Klauzula RETURNING ... INTO ...
Na przykład przy autogenerate dla klucza, którego nie znamy przy INSERT, możemy go pobrać z wyniku INSERTa bez kolejnych zapytań.

Nie działa na tabelach odległych (w rozproszonym systemie).

## Mniej znane instrukcje DML
### Ładowanie hurtowni danych
MERGE ... WHEN MATCHED THEN UPDATE ... WHEN NOT MATCHED THEN INSERT ...

Ładowanie i odświeżanie danych - polecam! Tomasz Traczyk

### Rozdzielanie danych do tabel w jednym przebiegu
INSERT ALL|FIRST WHEN ... THEN INTO ... (coś dalej)

## Zdania DDL w Oracle
Trochę inne niż u innych producentów.

Automatycznie się commitują.

Możliwości zmiany typów kolumn są ograniczone - brak jak tabela pusta lub w kolumnie same NULLe.

Dla kluczy głównych i unikalnych tworzone są automatyczne indeksy.

## Tworzenie tabel
Można dodawać komentarze i warto dla dokumentowania struktury danych.

```
COMMENT ON TABLE wykladowcy IS 'Dane wykładowców'
COMMENT ON COLUMN wykladowcy.id IS 'Identyfikator wykładowcy'
```

## Inne operacje
Przy usuwaniu można dodać na końcu CASCADE CONSTRAINTS, żeby wywalić klucze obce (potencjalnie też dane skojarzone tymi kluczami)

Zmiana, VARCHARa na przykład, tylko w górę.

Przy dodawaniu kolumny do tabeli z jakimiś wierszami, trzeba specyzować wartość domyślną lub NULL.

Ograniczeniom warto nadawać swoje nazwy.

Tworzenie tabeli -> Wrzut danych -> Dodanie ograniczeń (lepsze dla pamięci indeksów)

NULL nie może być w kluczu głównym.

Można włączać i wyłączać więzy, zamiast ich usuwania. Ograniczenie włączone gwarantuje spełnienie go przez wszystkie dane.

Tabela `exceptions` zawiera informacje o błędnych wierszach. Tworzenie takiej przez skrypt UTLEXCPT.SQL.

Można sterować chwilą sprawdzania więzów, np. na zaraz przed commitem: wartość DEFERRED. Zmienić kody w tabeli głównej i tej z nią skojarzonej kluczami obcymi.

Pseudokolumny NEXTVAL i CURRVAL.

## Indeksy
Baza ze źle zbudowanymi indeksami działa wolno.

Warto generować histogram wartości klucza indeksowego, żeby optymalizator zapytań wiedział lepiej czy dla danej wyszukiwanej wartości używać indeksu czy nie (bo jest odpowiednio rzadka lub częsta).

Mając indeks, mamy w zasadzie dwie tabele do złączenia posortowane w kolejności indeksu i szybko się dokonuje złączenia.

Indeksy spowaniają polecenia DML. Klucze główne i obce praktycznie zawsze ustawiamy, a inne po długim zastanowieniu i zwykle rezygnujemy.

Indeksy często w innym tablespace niż tabela. Uzasadnienia szczególnego nie ma.

## Grono (cluster)

Struktura, w której można umieścić kilka tabel, gdzie wiersze z tabel o tej samej wartości klucza są fizycznie blisko siebie - np. nagłówki faktur i pozycje faktur.

Przyspiesza dostęp wg jednego wzorca, ale psuje wszystkie inne - np. podaj wszystkich klientów, z fakturą wystawioną na adres przy Grójeckiej.