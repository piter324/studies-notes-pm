# Wykład 1
doc. dr inż. Tomasz Traczyk,
pok. 518,
tel. wewn. 7750,
email: ttraczyk@ia.pw.edu.pl

Wykład: czwartki 16:15-18:00

Projekt rozpocznie się po ok. 1 miesiącu, terminy oddawania etapów projektu określa instrukcja

Konsultacje: środa 16:15 zwykle, ale warto sprawdzić na stronie http://dydaktyka.ia.pw.edu.pl i mailem

Maile z listy z serwera studia z listy KBD2.

Konspekty na serwerze studia. Nie zawierają pełnej treści wykładów. Mogą się zmieniać treściowo.

Wykład przekazuje wiedzę ogólną z zakresu znajomości produktu i rozwiązywania problemów.

- Obecność na wykładach nie jest obowiązkowa. 
- Nie ma stałych godzin projektu, ale można się konsultować - wręcz należy.
- Zaliczenie na Studia3 lub w USOSie - elektronicznie dostarczone, może być pocztą.

Zaliczenie wymaga zaliczenia projektu i > 50% z kolosa. Jeden kolos 60 min. na ostatnim wykładzie. Nowa formuła i treść. Pytania problemowe. Czy można mieć notatki?

Projekt w zespołach 3-osobowych. Etapy:
- rozdanie tematów
- projekt wstępny - sprecyzowanie zadania i koncepcja rozwiązania - ZAL
- projekt struktur danych - ZAL
- odbiór końcowy - OCENA punktowa 0-35 (30p. + 5p. premii)
- oddanie projektu - dwa tygodnie przed końcem zajęć

Followować wytyczne nazewnictwa plików itd.

Terminy przestrzegać, ale nie ma pełnej spiny - nie tresuj się...

Jeśli będzie potrzebny kolos poprawkowy, to będzie przeprowadzony.

Poprawki do projektu albo w kolejnym jego etapie, albo jeśli będzie bardzo źle to w tym samym.

http://oracle.com/technetwork

## Częśc merytoryczna
Oracle - jedna z największych firm branży IT i największy producent systemów bazodanowych - udział w rynku ponad 40%.

Wykład głównie o RDBMS. Oracle ma też kilka innych systemów bazodanowych.

Projekt w Oracle JDeveloper. Użyjemy Oracle Application Express do stworzenia aplikacji webowych (urgh...). XML Publisher do tworzenia raportów biznesowych.

- Oracle Server Express Edition - darmowa, z ograniczeniami i bez opcji, starsza
- Oracle Server Standard Edition 2 - max. 2 procesory, bez opcji
- Oracle Server Enterprise Edition - pełna wersja